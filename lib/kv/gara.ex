defmodule KV.Gara do
  use GenServer

  def start_link do
   GenServer.start_link(__MODULE__, [], name: :gara)
  end

  def init(_) do
  IO.write "gara deschisa\n"

    {:ok, :ok}
  end

  def check() do
    GenServer.call(:triere, :test, 50000)
  end
  
end