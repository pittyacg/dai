defmodule KV.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: :supervisor)
  end

  def init(_) do
    children = [
       worker(KV.Triere, []),
      worker(KV.Gara, [])
      
     
    ]
 
    supervise(children, strategy: :one_for_one)

  end
end